package com.chat.client;

import java.net.*;
import java.io.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

class chatClient extends Frame implements Runnable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = -7323149453884506542L;
	Socket soc;    
    TextField tf;
    TextArea ta;
    Button btnSend,btnClose;
    String sendTo;
    String LoginName;
    Thread t=null;
    DataOutputStream dout;
    DataInputStream din;
    chatClient(String LoginName,String chatwith) throws Exception
    {
        super(LoginName);
        this.LoginName=LoginName;
        sendTo=chatwith;
        tf=new TextField(50);
        ta=new TextArea(50,50);
        btnSend=new Button("Send");
        btnClose=new Button("Close");
        soc=new Socket("127.0.0.1",5217);

        din=new DataInputStream(soc.getInputStream()); 
        dout=new DataOutputStream(soc.getOutputStream());        
        dout.writeUTF(LoginName);

        t=new Thread(this);
        t.start();

    }
    void sendtext(){
    	 try
         {  
             dout.writeUTF(sendTo + " "  + "DATA" + " " + tf.getText().toString());            
             ta.append("\n" + LoginName + " Says:" + tf.getText().toString());    
             tf.setText("");
         }
         catch(Exception ex)
         {
         }    
    }
    void setup()
    {
        setSize(600,400);
        setLayout(new GridLayout(2,1));

        add(ta);
        Panel p=new Panel();
        
        p.add(tf);
        p.add(btnSend);
        p.add(btnClose);
        add(p);
        tf.addKeyListener(new KeyAdapter() 
        {
            public void keyPressed(KeyEvent evt)
            {
                if(evt.getKeyCode() == KeyEvent.VK_ENTER)
                {
                    System.out.println("Pressed enter");
                    sendtext();
                   
                }
            }
        });
        
        show();        
    }
    public boolean action(Event e,Object o)
    {   System.out.println( e.key +":"+ e.ENTER);
        if(e.arg.equals("Send"))
        {
        	 sendtext();   
        }
        else if(e.arg.equals("Close"))
        {
            try
            {
                dout.writeUTF(LoginName + " LOGOUT");
                System.exit(1);
            }
            catch(Exception ex)
            {
            }
            
        }
        
        return super.action(e,o);
    }
    public static void main(String args[]) throws Exception
    {
        chatClient Client1=new chatClient("login2","login1");//(args[0],args[1]);
        Client1.setup();                
    }    
    public void run()
    {        
        while(true)
        {
            try
            {
                ta.append( "\n" + sendTo + " Says :" + din.readUTF());
                
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }
}
